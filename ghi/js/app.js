function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card-container" style="box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
        </div>
        <div>
            <ul class="list-group list-group-flush" style="border: 1px solid #dee2e6; margin-bottom: 20px;">
                <li class="list-group-item">
                    ${starts} - ${ends}
                </li>
            </ul>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
    const response = await fetch(url);

    if (!response.ok) {
        const alertContainer = document.getElementById('alert-container')
        const alertHTML = `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error!</strong> There was a problem fetching the URL.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `;
    alertContainer.innerHTML = alertHTML;

    } else {
        const data = await response.json();

        const container = document.querySelectorAll('.col-4');
        let index = 0;

        // for (let i = 0; i < container.length; i++){
        //     const column = container[i];

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = container[index];
                    column.innerHTML += html;
                    index = (index + 1) % 3;


        }
        }
        // }



    }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

});
