window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            //add error handling here
        } else {
            const data = await response.json();
            console.log(data);

            const selectTag = document.getElementById('location')
            console.log(selectTag);
            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
        }
    } catch {

    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const newConferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (newConferenceResponse.ok){
            formTag.reset();
            const newConfernce = await newConferenceResponse.json();
            console.log(newConfernce)



        }
    });
});
