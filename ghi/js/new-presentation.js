window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'

    try {
        const response = await fetch(url);

        if (!response.ok) {
            //add error handling here

        } else {
            const data = await response.json();
            console.log("data::::", data);

            const selectTag = document.getElementById('conference');
            // console.log(selectTag);
            for (let conference of data.conferences ){
                const option = document.createElement('option');
                option.value = conference.id;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
        }
    } catch {
        //erro catch here
    }

    const formTag = document.getElementById('create-presentation-form');
    const selectTag = document.getElementById('conference');
    console.log("tag:::", formTag);
    console.log("select:::", selectTag);

    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceId = selectTag;
        const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const newPresentationResponse = await fetch(conferenceUrl, fetchConfig);
        if (newPresentationResponse.ok){
            formTag.reset();
            const newPresentation = await newPresentationResponse.json();
            console.log("new:::", newPresentation);
        }

    });


});
